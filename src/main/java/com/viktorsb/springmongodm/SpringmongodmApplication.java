package com.viktorsb.springmongodm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringmongodmApplication implements CommandLineRunner {

	@Autowired
	private CustomerRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SpringmongodmApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		repository.deleteAll();

		// save few customers
		repository.save(new Customer("Jack", "Robertson"));
		repository.save(new Customer("May", "Go"));
		repository.save(new Customer("Ralph", "Bartlet"));
		repository.save(new Customer("Lana", "Go"));

		// fetch all customers
		System.out.println("Customers found with findAll():");
		System.out.println("-------------------------------");
		repository.findAll().forEach(System.out::println);
		System.out.println();

		// fetch an individual customer
		System.out.println("Customer found with findByFirstName('May')");
		System.out.println("------------------------------------------");
		System.out.println(repository.findByFirstName("May"));

		System.out.println("Customer found with findByLastName('Go')");
		System.out.println("----------------------------------------");
		repository.findByLastName("Go").forEach(System.out::println);
	}
}
